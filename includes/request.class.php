<?php

/*
 * @author: Jason Madrigal
 * @description: Request common actions wrapper
 */

namespace App\Http;

class Request {

    public function retrieveValueFromRequest($paramKey, $type) {
        if (!$paramKey || !$type) {
            return null;
        }
        $value = null;
        switch (strtoupper($type)) {
            case 'GET':
                $value = filter_input(INPUT_GET, $paramKey);
                break;
            case 'POST':
                $value = filter_input(INPUT_POST, $paramKey);
//                $value = $_POST[$paramKey];
                break;
            default:
                $value = null;
                break;
        }
        if(!$value)
        {
            return null;
        }
        return $this->cleanValue($value);
    }

    public function get($paramKey) {
        return $this->retrieveValueFromRequest($paramKey, 'get');
    }

    public function post($paramKey) {
        return $this->retrieveValueFromRequest($paramKey, 'post');
    }

    private function cleanValue($value) {
        $result = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
        $result = htmlentities($result, ENT_QUOTES, 'UTF-8');
        $result = htmlspecialchars($result, ENT_QUOTES, 'UTF-8');
        return $result;
    }

}
