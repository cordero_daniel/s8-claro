<?php
require('configure.php');

/****
 * All database linked operations are handled in this class
 **/
class DBManager
{

    var $serverName;
    var $userName;
    var $userPass;
    var $dbName;
    var $conn;
    var $numRows;
    var $home;

    function __construct()
    {
        $this->serverName = DB_HOST;
        $this->userName = DB_USER;
        $this->userPass = DB_PASSWORD;
        $this->dbName = DB_NAME;
        $this->home = HOME_URL;
    }

    function createConnection()
    {
        $this->conn = mysqli_connect($this->serverName, $this->userName, $this->userPass, $this->dbName);
        if (!$this->conn) {
            header("HTTP/1.0 512 Storage Issue");
            echo json_encode(['message' => 'Hemos tenido inconvenientes conectando con la base de datos.']);
            exit();
        }
//        mysqli_select_db($this->dbName)
//		$this->conn = mysql_connect($this->serverName,$this->userName,$this->userPass)  or die(mysql_error());
//		mysql_select_db($this->dbName,$this->conn) or die(mysql_error());
    }

    function getHomeUrl()
    {
        return $this->home;
    }


    function insertRecord($tblName, $fieldArray, $show = false)
    {
        if (is_array($fieldArray)) {
            $fieldNames = "";
            $fieldValues = "";
            foreach ($fieldArray as $field => $val) {
                if ($fieldNames == "") {
                    $fieldNames = $field;
                } else {
                    $fieldNames .= "," . $field;
                }
                if ($fieldValues == "") {
                    $fieldValues = "'$val'";
                } else {
                    $fieldValues .= "," . "'$val'";
                }
            }
            $qry = "insert into $tblName ($fieldNames) values ($fieldValues)";

            if ($show) {
                echo "Query --  " . $qry;
                exit;
            }
            return $this->exeQuery($qry);
        }
    }

    function getNum($consulta)
    {

        $rsQuery = $this->exeQuery($consulta);
        $numQuery = mysql_num_rows($rsQuery);
        return $numQuery;
    }

    function getNumRows($consulta)
    {

        $rsQuery = $this->exeQuery($consulta);
        $numQuery = mysql_num_rows($rsQuery);
        return $numQuery;
    }

    function updateRecord($tblName, $fieldArray, $where, $show = false)
    {
        //$fieldArray = $this->safeSqlEncode($fieldArray);
        $updatedValues = "";

        foreach ($fieldArray as $field => $val) {
            if ($updatedValues == "") {
                $updatedValues = $field . "='$val'";
            } else {
                $updatedValues .= "," . $field . "='$val'";
            }
        }

        $qry = "update $tblName set $updatedValues where $where";
        if ($show) {
            echo "Query --  " . $qry;
            exit;
        }
        $exe = $this->exeQuery($qry);
        if ($exe) {
            return true;
        }
    }

    function lastInsert()
    {
        $this->createConnection();
        $id = mysql_insert_id();
        $this->closeConnection($this->conn);
        return $id;
    }

    function exeQuery($qry)
    {
        $this->createConnection();
        $exe = mysqli_query($this->conn, $qry) or die(mysqli_error($this->conn));
        if (!is_bool($exe)) {
            $this->numRows = mysqli_num_rows($exe);
        }
        $this->closeConnection($this->conn);
        return $exe;
    }

    function fetchRecord($qry, $show = false)
    {
        $array = array();
        if ($show) {
            echo "Query --  " . $qry;
            exit;
        }
        $exe = $this->exeQuery($qry);
        while ($res = mysqli_fetch_assoc($exe)) {
            $array[] = $res;
        }
        return $array;
    }

    function InsertTheme($ThemeId, $paramPageId)
    {
        $qry = "INSERT into tbl_pages_tabs (vchPageId,intThemeId,vchTabLabel,vchTabApplicationName) values ('$paramPageId','$ThemeId','Welcome','signitywelcome')";
        $exe = $this->exeQuery($qry);
        if ($exe) {
            return true;
        }
    }

    function InsertTempID($ThemeId, $uid)
    {
        $qry = "INSERT into tbl_temp_User (vch_userID,intThemeId) values ('$uid','$ThemeId')";
        $exe = $this->exeQuery($qry);
        if ($exe) {
            return true;
        }
    }

    function DeleteRecord($tblname, $where, $show = false)
    {
        $qry = "delete from $tblname where $where";
        if ($show) {
            echo "Delete Query---" . $qry;
            exit;
        }
        $exe = $this->exeQuery($qry);
        if ($exe) {
            return true;
        }
    }

    function fetchRecordSet($qry, $show = false)
    {
        if ($show) {
            echo "Query --  " . $qry;
            exit;
        }
        $exe = $this->exeQuery($qry);
        return $exe;
    }

    function recordCount($qry, $show = false)
    {
        if ($show) {
            echo "Query --  " . $qry;
            exit;
        }
        $this->exeQuery($qry); //XXX/anoop. We don't need to get records for count.
        return $this->numRows;
    }

    public function getRecord($tblName, $fieldArray, $show = false, $modifier = "")
    {
        $fieldArray = $this->safeSqlEncode($fieldArray);
        //print_r($fieldArray);
        $where = "";
        foreach ($fieldArray as $field => $val) {
            if ($where == "") {
                $where = $field . "='$val'";
            } else {
                $where .= " and " . $field . "='$val'";
            }
        }
        if (count($fieldArray) == 0) {
            $where = 1;
        }
        $qry = "select * from $tblName where $where $modifier";
        if ($show) {
            return $qry;
        }
        return $this->fetchRecord($qry);
    }


    function safeSqlEncode($itemData)
    {
        $this->createConnection(); // need to do conn multiplexing.
        foreach ($itemData as $key => $val) {
            unset($itemData{$key});
            $key = $this->sqlSafe($key);
            $val = $this->sqlSafe($val);
            $itemData{$key} = $val;
        }
        return $itemData;
    }

    function safeSqlDecode($itemData)
    {
        $this->createConnection(); // need to do conn multiplexing.
        foreach ($itemData as $key => $val) {
            unset($itemData{$key});
            $order = array("\\r\\n", "\\r", "\\n");
            $val = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $val);
            $val = str_replace($order, "\r\n", $val);
            $key = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $key);
            $key = str_replace($order, "\r\n", $key);
            $itemData{$key} = $val;
        }
        return $itemData;
    }

    function sqlSafe($str)
    {
        if ($this->conn == null) {
            $this->createConnection();
        }
        $str = mysql_real_escape_string($str, $this->conn);
        return preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $str); // remove non printable characters.
    }


    function getLastId($tblName, $select)
    {
        $qry = "select MAX($select) as top from $tblName";
        $exe = $this->exeQuery($qry);
        $res = mysql_fetch_object($exe);
        return $res->top;
    }

    function closeConnection($connection)
    {
//		mysql_close($var);
        $connection->close();
    }


    function RTESafe($strText)
    {
        $tmpString = trim($strText);
        $tmpString = str_replace(chr(145), chr(39), $tmpString);
        $tmpString = str_replace(chr(146), chr(39), $tmpString);
        $tmpString = str_replace("'", "&#39;", $tmpString);
        $tmpString = str_replace(chr(147), chr(34), $tmpString);
        $tmpString = str_replace(chr(148), chr(34), $tmpString);
        $tmpString = str_replace(chr(10), " ", $tmpString);
        $tmpString = str_replace(chr(13), " ", $tmpString);
        return $tmpString;
    }

    // paging function
    function setPaging($query, $perPage, $currPage, $show = false)
    {
        if ($show) {
            echo $query;
            exit;
        }

        $rs = $this->exeQuery($query);
        $num_rows = $this->getNumRows();
        $perPage = $perPage;
        $totalPages = ceil($num_rows / $perPage);
        $page = $currPage;
        $start = ($page - 1) * $perPage;

        $query2 = " $query  limit $start ,$perPage";

        $rs2 = $this->exeQuery($query2);

        $returValue = array();
        $returValue['start'] = $start;
        $returValue['page'] = $page;
        $returValue['result'] = $rs2;
        $returValue['totalPages'] = $totalPages;
        return $returValue;
    }


    /***
     * This function returns theme's file name as applicable for this particular tab
     ***/

    function getTemplateName($paramTabId)
    {
        $strQuery = " SELECT vchThemeFile FROM tbl_pages_tabs,tbl_themes WHERE tbl_pages_tabs.intThemeId=tbl_themes.intThemeId and tbl_pages_tabs.intTabId='$paramTabId'";

        $rsQuery = $this->exeQuery($strQuery);
        while ($res = mysql_fetch_assoc($rsQuery)) {
            $strThemeFileName = $res['vchThemeFile'];
        }
        return $strThemeFileName;

    }

    /*function getAdminTemplateName($paramTabId)
    {
        $strQuery=" SELECT vchAdminThemeFile FROM tbl_pages_tabs,tbl_themes WHERE tbl_pages_tabs.intThemeId=tbl_themes.intThemeId and tbl_pages_tabs.intTabId=1";

        $rsQuery = $this->exeQuery($strQuery);
        while($res = mysql_fetch_assoc($rsQuery))
        {
            $strThemeFileName = $res['vchAdminThemeFile'];
        }
        return $strThemeFileName;

    }*/


    /***
     * This function returns theme's name value pair and store the same as array
     ***/

    function getFieldArrays($intThemeId, $intTabId)
    {

        //delcare an object of theme fields
        //$objThemeFields = new ThemeFields();
        $arrFieldNameValue = array();

        //$strQuery=" SELECT vchFieldName, vchFieldValue FROM tbl_tabs_fields_values, tbl_theme_config_fields WHERE tbl_tabs_fields_values.intFieldId = tbl_theme_config_fields.intFieldId AND tbl_theme_config_fields.intThemeId =101 LIMIT 0 , 30 ";
        $strQuery = " SELECT vchFieldName, vchFieldValue FROM tbl_tabs_fields_values RIGHT JOIN tbl_theme_config_fields ON tbl_tabs_fields_values.intFieldId = tbl_theme_config_fields.intFieldId WHERE tbl_theme_config_fields.intThemeId ='$intThemeId' and tbl_tabs_fields_values.intTabId = '$intTabId' LIMIT 0 , 30 ";

        $rsQuery = $this->exeQuery($strQuery);
        while ($res = mysql_fetch_assoc($rsQuery)) {
            $arrFieldNameValue[$res['vchFieldName']] = $res['vchFieldValue'];
        }
        return $arrFieldNameValue;

    }

    /***
     * This function returns theme's field name and type pair and store the same as array
     ***/

    function getFieldTypes()
    {

        $typeFieldNameValue = array();

        $strQuery = "SELECT intFieldId, vchFieldType FROM tbl_theme_config_fields WHERE intThemeId =101 LIMIT 0 , 30 ";

        $rsQuery = $this->exeQuery($strQuery);
        while ($res = mysql_fetch_assoc($rsQuery)) {
            $typeFieldNameValue[$res['intFieldId']] = $res['vchFieldType'];
        }

        return $typeFieldNameValue;
    }

    /***
     *
     * This function inserts the records into the tbl_tabs_fields_values table
     *
     ***/
    function insertIntoDB($keyValueFields, $intTabID)
    {

        //If array of value fields is empty then just print an error message
        if (empty($keyValueFields)) {

            die('There is no values to insert. There must be some errors');
        }
        foreach ($keyValueFields as $key => $value) {
            $conditionalData['intFieldId'] = $key;
            $conditionalData['intTabId'] = $intTabID;
            $contents = self:: getRecord('tbl_tabs_fields_values', $conditionalData);
            //CHANGE THE VIDEO URL
            if ($key == 9) {

                $splitArray = explode('http://www.youtube.com/watch?v=', $value);
                if (!strcmp($splitArray[0], $value)) {
                    $value = $value;
                } else {

                    $value = 'http://www.youtube.com/v/' . $splitArray[1];
                }
            }
            if (count($contents) >= 1) { //If record already exists then simply update otherwise insert a new entery

                if (!empty($value)) { //if empty then do not replace the values

                    $where = 'intFieldId=' . $key . ' and intTabId = ' . $intTabID;
                    self:: updateRecord('tbl_tabs_fields_values', array('vchFieldValue' => $value), $where);
                }
            } else {

                self:: insertRecord('tbl_tabs_fields_values', array('intFieldId' => $key, 'vchFieldValue' => $value, 'intTabId' => $intTabID));
            }
        }

    }

}//class db manager
//$objdbManager = new DBManager();
?>