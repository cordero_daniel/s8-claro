<?php
require('DBManager.php');

class ManagerActivaPack
{

    var $objDBManager;

    function __construct()
    {
    }

    function connect()
    {
        $this->objDBManager = new DBManager();
        $this->objDBManager->createConnection();
    }

//    function closeConnection()
//    {
//        $this->objDBManager->closeConnection($this->objDBManager->conn);
//    }

    function save($data)
    {
        $this->connect();
        date_default_timezone_set('America/Managua');

        $data['created_at'] = date('Y-m-d h:i:s');
        $query = $this->objDBManager->insertRecord('lead_form', $data);
        return $query;

    }

    public function updateLeadComments($leadId, $agentComments)
    {
        $this->connect();
        $result = $this->objDBManager -> updateRecord('lead_form', array('agent_comments'=>$agentComments), "ID=$leadId");
        return $result;
    }

    function limpiarValores($variable)
    {

        $resultado = $variable;
        $resultado = mb_convert_encoding($resultado, 'UTF-8', 'UTF-8');
        $resultado = htmlentities($resultado, ENT_QUOTES, 'UTF-8');

        return $resultado;

    }

}

?>