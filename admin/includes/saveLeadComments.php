<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include dirname(dirname(__DIR__)) . '/includes/ManagerActivaPack.php';

$objActivaPack = new ManagerActivaPack();

$agentComments = $objActivaPack->limpiarValores($_POST['agent_comments']);
$leadId = $objActivaPack->limpiarValores($_POST['leadId']);

if(!isset($agentComments) || !isset($leadId))
{
	exit('error');
}

$result = $objActivaPack->updateLeadComments($leadId, $agentComments);

if(!$result)
{
	exit('error');
}
else
{
	exit('true');
}