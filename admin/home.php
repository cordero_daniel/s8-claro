<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);
include_once dirname(__DIR__) . '/admin/includes/check_session.php';
include dirname(__DIR__) . '/includes/DBManager.php';
$objDBManager = new DBManager(); //initialize db connection

?>
<!doctype html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Administrador</title>
        <link href="css/estilo.css" rel="stylesheet" type="text/css"> 
        <link rel="stylesheet" type="text/css" href="css/admin_estilos.css">
        <link rel="stylesheet" type="text/css" href="css/stylesheet.css"> 

        <style type="text/css">
            .mostrarComentario{
                color:red;
            }
            .ocultarComentario{
                color:green;
            }
        </style>
        <style id="antiClickjack">body{display:none !important;}</style><script type="text/javascript">if (self === top) {var antiClickjack = document.getElementById("antiClickjack");antiClickjack.parentNode.removeChild(antiClickjack);} else {top.location = self.location;}</script>
    </head>
    <body>
        <div id="HeaderMain">
            <div class="FloatLeft">
                <ul>
                    <li><a href="salir.php">Salir</a></li>
                </ul>
            </div>
            <div class="FloatRight" id="float"><!--<img src="images/logo_movieleaks.png" width="300" alt="Admin Console" />--></div>
        </div>
        <div id="OuterBody">

            <div id="LeftPanel" style="float:left">
                <ul>
                    <li><a href="xls.php?type=1">Descargar</a></li>
                </ul>
            </div>
            <div id="RightPanel">
                <div id="frame">

                    <div id="content_blanco_datos">
                        <?php
                        
                        $query = "SELECT `country` FROM `users` WHERE `usuario` = '" . $_SESSION['login_name'] . "'";
                        $country = $objDBManager->fetchRecord($query)[0]['country'];

                        $query = "SELECT * FROM `lead_form` WHERE `country` = '" . $country . "'";
                        $registros = $objDBManager->fetchRecord($query);
                        
                        ?>

                        <p id="error" class="error"></p>
                        <table id="tabla_datos" cellspacing="1">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Email</th>
                                    <th>Teléfono</th>
                                    <th>Smart Phone</th>
                                    <th>Modelo</th>
                                    <th>Acepta Información</th>
                                    <th>Comentarios</th>

                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($registros as &$dato) { ?>
                                    <tr>
                                        <td><?php echo $dato['created_at']; ?></td>
                                        <td><?php echo html_entity_decode($dato['name']); ?></td>
                                        <td><?php echo html_entity_decode($dato['last_name']); ?></td>
                                        <td><?php echo html_entity_decode($dato['email']); ?></td>
                                        <td><?php echo $dato['phone']; ?></td>
                                        <td><?php echo $dato['smart_phone']; ?></td>
                                        <td><?php echo $dato['modelo']; ?></td>
                                        <td><?php echo $dato['acepto_informacion']; ?></td>
                                        <td><textarea name="agent_comments-<?php echo $dato['id']; ?>" id="agent_comments-<?php echo $dato['id']; ?>" class="agent_comments" cols="40" rows="4"><?php echo $dato['agent_comments']; ?></textarea></td>
                                        <td><button class="saveLeadChangesButton" id="<?php echo $dato['id']; ?>">Salvar</button></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
            <div class="clr"></div>
            <br /><br /><br />
            <div id="FooterMain">© Copyright 2016. All right reserved.</div>
        </div>

        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#tabla_datos').dataTable({
                    "bPaginate": true,
                    "bLengthChange": true,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": true,
                    "oLanguage": {
                        "sProcessing": "Procesando...",
                        "sLengthMenu": "Mostrar _MENU_ registros",
                        "sZeroRecords": "&nbsp;&nbsp;No se encontraron resultados",
                        "sInfo": "&nbsp;&nbsp;&nbsp;Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                        "sInfoEmpty": "&nbsp;&nbsp;Mostrando desde 0 hasta 0 de 0 registros",
                        "sInfoFiltered": "(filtrado de _MAX_ registros en total)",
                        "sInfoPostFix": "",
                        "sSearch": "Buscar:",
                        "sUrl": "",
                        "oPaginate": {
                            "sFirst": "Primero",
                            "sPrevious": "&lt; Anterior",
                            "sNext": "Siguiente &gt;",
                            "sLast": "&Uacute;ltimo"
                        }
                    }
                });
            });
        </script>
    </body>
</html>
