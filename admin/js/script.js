(function($){
	// $('.tabla_asistencia').on('change', 'input[type=checkbox]', function(event){
	// 	console.log('asistencia check');
	// 	console.log('event', event);
	// 	console.log('element', this);
	// 	console.log('checked', $(this).is(':checked'));
	// 	var idAssistant = $(this).attr('idassistant'),
	// 		checked = $(this).is(':checked') ? 1 : 0;
		// $.post('includes/saveLeadComments.php', {'idassistant': idAssistant, 'checked': checked}, function(data, textStatus, jqXHR){
		// 	console.log('data', data);
		// 	console.log('textStatus', textStatus);
		// 	console.log('jqXHR', jqXHR);
		// 	if(data === 'error')
		// 	{
		// 		alert('No se ha podido actualizar la información');
		// 	}
		// });
	// });

    $('.saveLeadChangesButton').on('click', function(event){
        $('*').css('cursor', 'wait');
        var $this = $(this),
            id = $this.attr('id'),
            comments = $('#agent_comments-'+id).val();
        $.post('includes/saveLeadComments.php', {'agent_comments': comments, 'leadId': id}, function(data, textStatus, jqXHR){
            $('*').css('cursor', 'auto');
            if(data === 'error')
            {
                alert('No se ha podido actualizar la información');
            }
            else
            {
                alert('El registro se actualizó correctamente');
            }
        });
    });

})(jQuery);