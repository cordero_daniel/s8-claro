<?php
//die(md5('Admin1++'));
ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
 <link rel="stylesheet" type="text/css" href="css/stylesheet.css"> 
<style id="antiClickjack">body{display:none !important;}</style>
    <script type="text/javascript">
       if (self === top) {
           var antiClickjack = document.getElementById("antiClickjack");
           antiClickjack.parentNode.removeChild(antiClickjack);
       } else {
           top.location = self.location;
       }
    </script>
</head>

<body onload="load_default();">
<script type="text/javascript">
function load_default() {
    document.loginfrm.userid.focus();
}
</script>
<!-- Header -->
<div id="HeaderMain">
<!-- Set page to minimum resolution 800 -->
<div><img src="images/spacer.gif" width="779" height="1" /></div>
	<div class="FloatLeft">
    	<ul>
        	<li class="Select"><a href="javascript:;">Login</a></li>
        </ul>
    </div>
    <div class="FloatRight"><!--<img src="images/logo.png" alt="Admin Console" />--></div>
</div>
<!-- Header End -->
<!-- Body -->
<div id="OuterBody" class="BodyPadding">
	<form name="loginfrm"  method="post" action="entrar.php" onsubmit="return check();">
	<table width="55%" align="center" border="0" cellspacing="1" cellpadding="0">
      <tr>
        <th colspan="2">Login</th>     
      </tr>
      <tr>
        <td width="40%" class="Odd" align="right">Username:</td>
        <td width="60%" class="Even Required"><input name="userid" id="userid" type="text" /></td>
      </tr>
      <tr>
        <td class="Odd" align="right">Password:</td>
        <td class="Even Required"><input name="password" id="password" type="password" /></td>
      </tr>
	  <input type="hidden" name="submit" value="submit" />
      <tr>
        <td class="Odd" align="right">&nbsp;</td>
        <td class="Even"><input type="image" name="imageField" id="imageField" src="images/submit-btn.gif" alt="Submit" title="Submit" /> <input type="image" name="imageField2" id="imageField2" src="images/cancel-btn.gif" alt="Cancel" title="Cancel" /></td>
      </tr>
      <tr>
        <td class="Odd Error">I Required Fields</td>
        <td class="Odd Error"></td>
      </tr>
    </table>
    </form>
</div>
<!-- Body End -->
<!-- Footer -->
<div id="FooterMain"> © Copyright 2014. All right reserved.</div>
<!-- Footer End -->
</body>
</html>
<?php
ob_end_flush();
?>