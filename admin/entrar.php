<?php

// error_reporting(E_ALL);
// ini_set('display_errors', 1);

include dirname(__DIR__) . '/includes/DBManager.php';
//
$objDBManager = new DBManager(); //initialize db connection
$objDBManager->createConnection();
//var_dump(isset($_POST['submit']) && limpiarCaracteresPost($_POST['submit']));
if (isset($_POST['submit']) && limpiarCaracteresPost($_POST['submit'])) {
    $user = limpiarCaracteresPost($_POST['userid']);
    $pass = md5(limpiarCaracteresPost($_POST['password']));
    if ($user == '' || $pass == '') {
        header('location:index.php');
    }

    $query = "select * from users where usuario='" . $user . "' and pass='" . $pass . "'";
    $results = $objDBManager->fetchRecordSet($query);
    if ($reg = mysqli_fetch_array($results)) {
        session_start();
        $_SESSION['login_name'] = $user;
        header('location:home.php');
    } else {
        header('location:index.php');
    }
} else {
    header('location:index.php');
}

function limpiarCaracteresPost($variable) {
    $result = 0;
    $result = mb_convert_encoding($variable, 'UTF-8', 'UTF-8');
    $result = htmlentities($result, ENT_QUOTES, 'UTF-8');
    $result = strip_tags($result);
    $result = htmlspecialchars($result);

    return $result;
}
