// Wait for the DOM to be ready
$(function() {
	// Initialize form validation on the registration form.
	$('#btn_enviar').on('click', function() {
		// It has the name attribute "registration"
		$("form[name='unboxingS8']").validate({
			// Specify validation rules
			rules : {
				// The key name on the left side is the name attribute
				// of an input field. Validation rules are defined
				// on the right side
				txt_nombre : "required",
				txt_apellido : "required",
				txt_phone : "required",
				txt_email : {
					required : true,
					// Specify that email should be validated
					// by the built-in "email" rule
					email : true
				}	
			},
			// Specify validation error messages
			messages : {
				txt_nombre : "Por favor ingrese su nombre",
				txt_apellido : "Por favor ingrese su apellido",
				txt_phone : "Por favor ingrese su número de teléfono",
				txt_email : "Por favor ingrese una dirección de correo válida",
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler : function(form) {
				form.submit();
			}
		});
	});

	//only letters on nombrecompleto
	$('#txt_nombre').keypress(function(e) {
		var regex = new RegExp("^[a-zA-Z\b\\s]$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});
	
	$('#txt_apellido').keypress(function(e) {
		var regex = new RegExp("^[a-zA-Z\b\\s]$");
		var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
		if (regex.test(str)) {
			return true;
		}

		e.preventDefault();
		return false;
	});
	//only numbers on telefono
	$(".number").keydown(function(e) {

		if (event.shiftKey) {
			event.preventDefault();
		}

		if (event.keyCode == 46 || event.keyCode == 8) {

		} else {
			if (event.keyCode < 95) {
				if (event.keyCode < 48 || event.keyCode > 57) {
					event.preventDefault();
				}
			} else {
				if (event.keyCode < 96 || event.keyCode > 105) {
					event.preventDefault();
				}
			}
		}
	});
});

