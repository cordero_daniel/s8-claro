<?php
/**
 *
 * User: jason.madrigal
 * Date: 10/11/16
 * Time: 3:48 PM
 */
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

include dirname(__DIR__) . '/includes/request.class.php';
include dirname(__DIR__) . '/includes/ManagerActivaPack.php';

use App\Http\Request as Request;

//header('Content-Type: application/json');

function isEmpty($value) {
    return empty($value);
}

function validateEmail($value) {
    if (empty($value)) {
        return false;
    }
    if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
        return false;
    }
    return true;
}

function save(Request $request)
{
    try
    {
        $name = $request->post('txt_nombre');
        $lastName = $request->post('txt_apellido');
        $email = $request->post('txt_email');
        $phoneNumber = $request->post('txt_phone');
        $country = $request->post('drp_pais');
        $smartPhone = $request->post('radio_smrt');
        $modelo = $request->post('radio_modelo');
        $informacion = $request->post('radio_contactame');
        
        $dbManager = new ManagerActivaPack();
        $data = [
            'name' => $name,
            'last_name' => $lastName,
            'email' => $email,
            'phone' => $phoneNumber,
            'country' => $country,
            'smart_phone' => $smartPhone,
            'modelo' => $modelo,
            'acepto_informacion' => $informacion
        ];
        $result = $dbManager->save($data);
        if (!$result) {
            header("HTTP/1.0 512 Storage Issue");
            echo "Hemos tenido inconvenientes guardando la información.";
            exit();
        }
        header("HTTP/1.0 200 OK");
        header("Location: ".HOME_URL."thank-you.html");
        exit();
    }
    catch (Exception $e)
    {
        header("HTTP/1.0 500 Internal Server Error ".$e->getTraceAsString());
        echo $e->getTraceAsString();
        exit();
    }
}

try{
    $request = new Request();
    $action = $request->get('action');
    switch ($action)
    {
        case 'save': {
            save($request);
            break;
        }
        default: {
            header("HTTP/1.0 400 Bad Request");
            exit();
            break;
        }
    }
    header("HTTP/1.0 400 Bad Request");
    exit();

}catch (Exception $e){
    header("HTTP/1.0 500 Internal Server Error ".$e->getTraceAsString());
    echo $e->getTraceAsString();
    exit();
}